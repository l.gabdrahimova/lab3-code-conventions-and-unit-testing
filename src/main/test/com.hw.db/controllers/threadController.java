package com.hw.db.controllers;

import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.*;
import com.hw.db.models.Thread;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.sql.Timestamp;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class threadControllerTests {
    private Thread thread;
    private String SLUG = "slug";

    @BeforeEach
    @DisplayName("test - thread creation ")
    void createThreadTest() {
        thread = new Thread("user1", new Timestamp(0), "forum1", "message1", "slug1", "title1", 1);
        thread.setId(0);
    }

    @Test
    @DisplayName("test - checking by id and slug")
    void testCheckIdOrSlug() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadController tc = new threadController();

            threadMock.when(() -> ThreadDAO.getThreadById(0)).thenReturn(thread);
            threadMock.when(() -> ThreadDAO.getThreadBySlug(SLUG)).thenReturn(thread);

            assertEquals(thread, tc.CheckIdOrSlug(SLUG));
            assertEquals(thread, tc.CheckIdOrSlug("0"));
        }
    }

    @Test
    @DisplayName("test - create post")
    void testCreatePost() {
        List<Post> posts = new ArrayList<>();
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadController tc = new threadController();

            threadMock.when(() -> ThreadDAO.getThreadBySlug(SLUG)).thenReturn(thread);

            assertEquals(ResponseEntity.status(HttpStatus.CREATED).body(posts), tc.createPost(SLUG, posts));
        }
    }

    @Test
    @DisplayName("test - get posts")
    void testGetPosts() {
        List<Post> posts = new ArrayList<>();
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadController tc = new threadController();

            threadMock.when(() -> ThreadDAO.getPosts(thread.getId(), 100, 1, "sort", true)).thenReturn(posts);
            threadMock.when(() -> ThreadDAO.getThreadBySlug(SLUG)).thenReturn(thread);

            assertEquals(ResponseEntity.status(HttpStatus.OK).body(posts), tc.Posts(SLUG, 100, 1, "sort", true));
        }
    }

    @Test
    @DisplayName("test - change")
    void testChange() {
        Thread changedThread = new Thread("user2", new Timestamp(100), "forum", "message2", "slug2", "title2", 11);
        changedThread.setId(1);

        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadController tc = new threadController();

            threadMock.when(() -> ThreadDAO.getThreadBySlug("slug2")).thenReturn(changedThread);
            threadMock.when(() -> ThreadDAO.getThreadById(1)).thenReturn(thread);

            assertEquals(ResponseEntity.status(HttpStatus.OK).body(thread), tc.change("slug2", thread), "thread was changed");
        }
    }

    @Test
    @DisplayName("test - info")
    void testInfo() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadController tc = new threadController();

            threadMock.when(() -> ThreadDAO.getThreadBySlug(SLUG)).thenReturn(thread);

            assertEquals(ResponseEntity.status(HttpStatus.OK).body(thread), tc.info(SLUG));
        }
    }

    @Test
    @DisplayName("test - voting")
    void testCreateVote() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
                User user = new User("user1", "email@mail.ru", "fullname", "abt");
                Vote vote = new Vote("user1", 1);
                threadController tc = new threadController();

                threadMock.when(() -> ThreadDAO.getThreadBySlug(SLUG)).thenReturn(thread);
                userMock.when(() -> UserDAO.Info("user1")).thenReturn(user);

                assertEquals(ResponseEntity.status(HttpStatus.OK).body(thread).getStatusCode(), tc.createVote(SLUG, vote).getStatusCode());
            }
        }
    }
}